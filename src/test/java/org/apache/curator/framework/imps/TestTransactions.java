package org.apache.curator.framework.imps;

import com.google.common.collect.Iterables;
import org.apache.curator.framework.*;
import org.apache.curator.framework.api.transaction.CuratorTransactionResult;
import org.apache.curator.framework.api.transaction.OperationType;
import org.apache.curator.framework.imps.CuratorFrameworkImpl;
import org.apache.curator.retry.RetryOneTime;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class TestTransactions {

//    @Test
//    public void testWithNamespace() throws Exception
//    {
//        try
//        {
//            if(CURATOR_FRAMEWORK.checkExists().forPath("/"+CORPORATE_ID)!= null) {
//                CURATOR_FRAMEWORK.delete().deletingChildrenIfNeeded().forPath("/"+CORPORATE_ID);
//            }
//
//            CuratorFramework client = CURATOR_FRAMEWORK.usingNamespace(CORPORATE_ID);
//            Collection<CuratorTransactionResult> results =
//                    client.inTransaction()
//                            .create().forPath("/foo", "one".getBytes())
//                            .and()
//                            .create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath("/test-", "one".getBytes())
//                            .and()
//                            .setData().forPath("/foo", "two".getBytes())
//                            .and()
//                            .create().forPath("/foo/bar")
//                            .and()
//                            .delete().forPath("/foo/bar")
//                            .and()
//                            .commit();
//
//            Assert.assertTrue(client.checkExists().forPath("/foo") != null);
//            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+"/foo") != null);
//            Assert.assertEquals(new String(client.getData().forPath("/foo")), "two");
//            Assert.assertTrue(client.checkExists().forPath("/foo/bar") == null);
//
//            CuratorTransactionResult    ephemeralResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/test-"));
//            Assert.assertNotNull(ephemeralResult);
//            Assert.assertNotEquals(ephemeralResult.getResultPath(), "/test-");
//            Assert.assertTrue(ephemeralResult.getResultPath().startsWith("/test-"));
//        }
//        finally
//        {
////            CURATOR_FRAMEWORK.delete().forPath("/foo");
//        }
//    }

    @Test
    public void     testWithNamespace2() throws Exception
    {
        CuratorFramework client = CuratorFrameworkFactory.builder().connectString("192.168.99.101:2181")
                .retryPolicy(new RetryOneTime(1))
                .namespace("galt").build();
        client.start();
        try
        {
            Collection<CuratorTransactionResult> results =
                    client.inTransaction()
                            .create().forPath("/foo", "one".getBytes())
                            .and()
                            .create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath("/test-", "one".getBytes())
                            .and()
                            .setData().forPath("/foo", "two".getBytes())
                            .and()
                            .create().forPath("/foo/bar")
                            .and()
                            .delete().forPath("/foo/bar")
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath("/foo") != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/galt/foo") != null);
            Assert.assertEquals(new String(client.getData().forPath("/foo")), "two");
            Assert.assertTrue(client.checkExists().forPath("/foo/bar") == null);

            CuratorTransactionResult    ephemeralResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/test-"));
            Assert.assertNotNull(ephemeralResult);
            Assert.assertNotEquals(ephemeralResult.getResultPath(), "/test-");
            Assert.assertTrue(ephemeralResult.getResultPath().startsWith("/test-"));
        }
        finally
        {
            client.delete().forPath("/foo");
            client.close();
        }
    }


    @Test
    public void     testCheckVersion() throws Exception
    {
        CuratorFramework        client = CuratorFrameworkFactory.newClient("192.168.99.101:2181", new RetryOneTime(1));
        client.start();
        try
        {
            if(client.checkExists().forPath("/foo")!= null)
                client.delete().deletingChildrenIfNeeded().forPath("/foo");
            client.create().forPath("/foo");
            Stat stat = client.setData().forPath("/foo", "new".getBytes());  // up the version

            try
            {
                client.inTransaction()
                        .check().withVersion(stat.getVersion() + 1).forPath("/foo") // force a bad version
                        .and()
                        .create().forPath("/bar")
                        .and()
                        .commit();

                Assert.fail();
            }
            catch ( KeeperException.BadVersionException correct )
            {
                // correct
            }

            Assert.assertNull(client.checkExists().forPath("/bar"));
        }
        finally
        {
//            client.delete().deletingChildrenIfNeeded().forPath("/foo");
            client.close();
        }
    }

    @Test
    public void     testBasic() throws Exception
    {
        CuratorFramework  client = CuratorFrameworkFactory.newClient("192.168.99.101:2181", new RetryOneTime(1));
        client.start();
        try
        {
            Collection<CuratorTransactionResult>    results =
                    client.inTransaction()
                            .create().forPath("/foo2")
                            .and()
                            .create().forPath("/foo2/bar", "snafu".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath("/foo2/bar") != null);
            Assert.assertEquals(new String(client.getData().forPath("/foo2/bar")), "snafu");

            CuratorTransactionResult    fooResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/foo2"));
            CuratorTransactionResult    fooBarResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/foo2/bar"));
            Assert.assertNotNull(fooResult);
            Assert.assertNotNull(fooBarResult);
            Assert.assertNotSame(fooResult, fooBarResult);
            Assert.assertEquals(fooResult.getResultPath(), "/foo2");
            Assert.assertEquals(fooBarResult.getResultPath(), "/foo2/bar");
        }
        finally
        {
            client.delete().deletingChildrenIfNeeded().forPath("/foo2");
            client.close();
        }
    }

//    @Test
//    public void testGuaranteedDeleteOnNonExistentNodeInForeground() throws Exception
//    {
//        CuratorFramework  client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", new RetryOneTime(1));
//        client.start();
//
//        final AtomicBoolean pathAdded = new AtomicBoolean(false);
//
//        ((CuratorFrameworkImpl)client).getFailedDeleteManager().debugListener = new FailedDeleteManagerListener()
//        {
//
//            @Override
//            public void pathAddedForDelete(String path)
//            {
//                pathAdded.set(true);
//            }
//        };
//
//        try
//        {
//            client.delete().guaranteed().forPath("/nonexistent");
//            Assert.fail();
//        }
//        catch(NoNodeException e)
//        {
//            //Exception is expected, the delete should not be retried
//            Assert.assertFalse(pathAdded.get());
//        }
//        finally
//        {
//            client.close();
//        }
//    }

    /**
     * Test that we are actually connected every time that we block until connection is established in a tight loop.
     */
    @Test
    @Ignore
    public void testBlockUntilConnectedTightLoop() throws InterruptedException
    {
        CuratorFramework client;
        for(int i = 0 ; i < 50 ; i++)
        {
            client = CuratorFrameworkFactory.newClient("192.168.99.101:2181", new RetryOneTime(100));
            try
            {
                client.start();
                client.blockUntilConnected();

                Assert.assertTrue("Not connected after blocking for connection #" + i, client.getZookeeperClient().isConnected());
            }
            finally
            {
                client.close();
            }
        }
    }


    @Test
    @Ignore
    public void testBlockUntilConnectedInterrupt()
    {
        //Kill the server
//        CloseableUtils.closeQuietly(server);

        final CuratorFramework client = CuratorFrameworkFactory.builder().
                connectString("192.168.99.101:2181").
                retryPolicy(new RetryOneTime(1)).
                build();

        try
        {
            client.start();

            final Thread threadToInterrupt = Thread.currentThread();

            Timer timer = new Timer();
            timer.schedule(new TimerTask()
            {

                @Override
                public void run()
                {
                    threadToInterrupt.interrupt();
                }
            }, 3000);

            client.blockUntilConnected(5, TimeUnit.SECONDS);
            Assert.fail("Expected interruption did not occur");
        }
        catch ( InterruptedException e )
        {
            //This is expected
        }
        finally
        {
            CloseableUtils.closeQuietly(client);
        }
    }

}
