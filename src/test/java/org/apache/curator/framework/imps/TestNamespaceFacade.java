/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.curator.framework.imps;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryOneTime;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.KeeperException.NoAuthException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class TestNamespaceFacade
{

    private static final CuratorFramework CURATOR_FRAMEWORK = CuratorFrameworkFactory.newClient("192.168.99.101:2181", new RetryOneTime(1));

    @BeforeClass
    public static void setUp()
    {
        CURATOR_FRAMEWORK.start();
    }

    @AfterClass
    public static void tearDown()
    {
        CloseableUtils.closeQuietly(CURATOR_FRAMEWORK);
    }

    @Test
    public void  testInvalid() throws Exception
    {
        try
        {
            CuratorFrameworkFactory.builder().namespace("/snafu").retryPolicy(new RetryOneTime(1)).connectString("foo").build();
            Assert.fail();
        }
        catch ( IllegalArgumentException e )
        {
            // correct
        }
    }

    @Test
    public void  testGetNamespace() throws Exception
    {
        CuratorFramework CURATOR_FRAMEWORK2 = CuratorFrameworkFactory.builder().namespace("snafu").retryPolicy(new RetryOneTime(1)).connectString("foo").build();
        try
        {

            CuratorFramework fooClient = CURATOR_FRAMEWORK.usingNamespace("foo");
            CuratorFramework barClient = CURATOR_FRAMEWORK.usingNamespace("bar");

            Assert.assertEquals(CURATOR_FRAMEWORK.getNamespace(), "");
            Assert.assertEquals(CURATOR_FRAMEWORK2.getNamespace(), "snafu");
            Assert.assertEquals(fooClient.getNamespace(), "foo");
            Assert.assertEquals(barClient.getNamespace(), "bar");
        }
        finally
        {
            CloseableUtils.closeQuietly(CURATOR_FRAMEWORK2);
        }
    }

    @Test
    public void  testSimultaneous() throws Exception
    {
        try
        {
            CuratorFramework fooClient = CURATOR_FRAMEWORK.usingNamespace("foo");
            CuratorFramework barClient = CURATOR_FRAMEWORK.usingNamespace("bar");

            fooClient.newNamespaceAwareEnsurePath("/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());
            barClient.newNamespaceAwareEnsurePath("/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());

            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/foo/one", false));
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/bar/one", false));
        }
        finally
        {
        }
    }

    @Test
    public void  testCache() throws Exception
    {
        try
        {
            Assert.assertSame(CURATOR_FRAMEWORK.usingNamespace("foo"), CURATOR_FRAMEWORK.usingNamespace("foo"));
            Assert.assertNotSame(CURATOR_FRAMEWORK.usingNamespace("foo"), CURATOR_FRAMEWORK.usingNamespace("bar"));
        }
        finally
        {
        }
    }

    @Test
    public void  testBasic() throws Exception
    {
        try
        {

            CURATOR_FRAMEWORK.newNamespaceAwareEnsurePath("/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/one", false));

            CURATOR_FRAMEWORK.newNamespaceAwareEnsurePath("/space/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/space", false));

            CURATOR_FRAMEWORK.newNamespaceAwareEnsurePath("/name/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/name", false));
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/name/one", false));
        }
        finally
        {
        }
    }

    /**
     * CURATOR-128: access root node within a namespace.
     */
    @Test
    public void  testRootAccess() throws Exception
    {
        try
        {
            CURATOR_FRAMEWORK.newNamespaceAwareEnsurePath("/one").ensure(CURATOR_FRAMEWORK.getZookeeperClient());
            Assert.assertNotNull(CURATOR_FRAMEWORK.getZookeeperClient().getZooKeeper().exists("/one", false));

            Assert.assertNotNull(CURATOR_FRAMEWORK.checkExists().forPath("/"));
            try
            {
                CURATOR_FRAMEWORK.checkExists().forPath("");
                Assert.fail("IllegalArgumentException expected");
            }
            catch ( IllegalArgumentException expected )
            {
            }

            Assert.assertNotNull(CURATOR_FRAMEWORK.usingNamespace("one").checkExists().forPath("/"));
            try
            {
                CURATOR_FRAMEWORK.usingNamespace("one").checkExists().forPath("");
                Assert.fail("IllegalArgumentException expected");
            }
            catch ( IllegalArgumentException expected )
            {
            }
        }
        finally
        {
        }
    }


    @Test
    public void  testIsStarted() throws Exception
    {
        CuratorFramework    namespaced = CURATOR_FRAMEWORK.usingNamespace(null);
        Assert.assertEquals("Namespaced state did not match true state after call to start." , CURATOR_FRAMEWORK.getState(), namespaced.getState());

        Assert.assertEquals( "Namespaced state did not match true state after call to close.", CURATOR_FRAMEWORK.getState(), namespaced.getState());
    }
    
    /**
     * Test that ACLs work on a NamespaceFacade. See CURATOR-132
     * @throws Exception
     */
    @Test
    public void TestNamespaceFacadetestACL() throws Exception
    {
        CURATOR_FRAMEWORK.getZookeeperClient().blockUntilConnectedOrTimedOut();

        //cleaning first
        CURATOR_FRAMEWORK.delete().deletingChildrenIfNeeded().forPath("/parent/child");
        CURATOR_FRAMEWORK.create().creatingParentsIfNeeded().forPath("/parent/child", "A string".getBytes());

        CuratorFramework CURATOR_FRAMEWORK2 = CURATOR_FRAMEWORK.usingNamespace("parent");

        Assert.assertNotNull(CURATOR_FRAMEWORK2.getData().forPath("/child"));  
        CURATOR_FRAMEWORK.setACL().withACL(Collections.singletonList(
            new ACL(ZooDefs.Perms.WRITE, ZooDefs.Ids.ANYONE_ID_UNSAFE))).
                forPath("/parent/child");
        // This will attempt to setACL on /parent/child, Previously this failed because /child
        // isn't present. Using "child" would case a failure because the path didn't start with
        // a slash
        try
        {
            List<ACL> acls = CURATOR_FRAMEWORK2.getACL().forPath("/child");
            Assert.assertNotNull(acls);
            Assert.assertEquals(acls.size(), 1);
            Assert.assertEquals(acls.get(0).getId(), ZooDefs.Ids.ANYONE_ID_UNSAFE);
            Assert.assertEquals(acls.get(0).getPerms(), ZooDefs.Perms.WRITE);
            CURATOR_FRAMEWORK2.setACL().withACL(Collections.singletonList(
                new ACL(ZooDefs.Perms.DELETE, ZooDefs.Ids.ANYONE_ID_UNSAFE))).
                    forPath("/child");
            Assert.fail("Expected auth exception was not thrown");
        }
        catch(NoAuthException e)
        {
            //Expected
        }
    }

    @Test
    public void testUnfixForEmptyNamespace() {
        CuratorFramework client = CuratorFrameworkFactory.builder().namespace("").retryPolicy(new RetryOneTime(1)).connectString("foo").build();
        CuratorFrameworkImpl CURATOR_FRAMEWORKImpl = (CuratorFrameworkImpl) client;

        Assert.assertEquals(CURATOR_FRAMEWORKImpl.unfixForNamespace("/foo/bar"), "foo/bar");

        CloseableUtils.closeQuietly(client);
    }
}
