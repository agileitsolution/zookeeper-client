package ie.ais.zkeeper.client;

import com.google.common.collect.Iterables;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.transaction.CuratorTransactionResult;
import org.apache.curator.framework.api.transaction.OperationType;
import org.apache.curator.retry.RetryOneTime;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;
import org.junit.*;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class TestTransactions {


    private static final CuratorFramework CURATOR_FRAMEWORK = CuratorFrameworkFactory.newClient("192.168.99.101:2181", new RetryOneTime(1));

    public static final String CORPORATE_ID = "corp123";
    public static final String BAN123 = "/BAN123";
    public static final String BAN456 = "/BAN456";

    public static final String DACMNO = "/DACMNO";
    public static final String DACPQR = "/DACPQR";

    public static final String SHOP1 = "/SHOP1";
    public static final String SHOP2 = "/SHOP2";

    @BeforeClass
    public static void setUp()
    {
        CURATOR_FRAMEWORK.start();
    }

    @AfterClass
    public static void tearDown()
    {
        CloseableUtils.closeQuietly(CURATOR_FRAMEWORK);
    }
    



    @Test
    public void testShopsProfile() throws Exception
    {
        try
        {
            if(CURATOR_FRAMEWORK.checkExists().forPath("/"+CORPORATE_ID)!= null) {
                CURATOR_FRAMEWORK.delete().deletingChildrenIfNeeded().forPath("/"+CORPORATE_ID);
            }

            CuratorFramework client = CURATOR_FRAMEWORK.usingNamespace(CORPORATE_ID);
            Collection<CuratorTransactionResult> results =
                    client.inTransaction()
                            .create().forPath(BAN123, "one".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN123) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN123) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN123)), "one");

            results =
                    client.inTransaction()
                            .create().forPath(BAN123 +DACMNO, "one".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN123 +DACMNO) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN123 +DACMNO) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN123 +DACMNO)), "one");

            results =
                    client.inTransaction()
                            .create().forPath(BAN123 + DACMNO + DACPQR, "one".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN123 + DACMNO + DACPQR) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN123 + DACMNO + DACPQR) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN123 + DACMNO + DACPQR)), "one");

            results =
                    client.inTransaction()
                            .create().forPath(BAN123 + DACMNO + DACPQR +SHOP1, "one".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN123 + DACMNO + DACPQR + SHOP1 ) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN123 + DACMNO + DACPQR+ SHOP1 ) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN123 + DACMNO + DACPQR+ SHOP1 )), "one");

            results =
                    client.inTransaction()
                            .create().forPath(BAN456, "one".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN456) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN456) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN456)), "one");

            results =
                    client.inTransaction()
                            .create().forPath(BAN456 +SHOP2, "two".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(client.checkExists().forPath(BAN456 +SHOP2 ) != null);
            Assert.assertTrue(client.usingNamespace(null).checkExists().forPath("/"+CORPORATE_ID+ BAN456 +SHOP2) != null);
            Assert.assertEquals(new String(client.getData().forPath(BAN456 +SHOP2)), "two");


        }
        finally
        {
//            CURATOR_FRAMEWORK.delete().forPath("/foo");
        }
    }



    @Test
    public void testBasic() throws Exception
    {
        try
        {
            Collection<CuratorTransactionResult>    results =
                    CURATOR_FRAMEWORK.inTransaction()
                            .create().forPath("/foo2")
                            .and()
                            .create().forPath("/foo2/bar", "snafu".getBytes())
                            .and()
                            .commit();

            Assert.assertTrue(CURATOR_FRAMEWORK.checkExists().forPath("/foo2/bar") != null);
            Assert.assertEquals(new String(CURATOR_FRAMEWORK.getData().forPath("/foo2/bar")), "snafu");

            CuratorTransactionResult    fooResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/foo2"));
            CuratorTransactionResult    fooBarResult = Iterables.find(results, CuratorTransactionResult.ofTypeAndPath(OperationType.CREATE, "/foo2/bar"));
            Assert.assertNotNull(fooResult);
            Assert.assertNotNull(fooBarResult);
            Assert.assertNotSame(fooResult, fooBarResult);
            Assert.assertEquals(fooResult.getResultPath(), "/foo2");
            Assert.assertEquals(fooBarResult.getResultPath(), "/foo2/bar");
        }
        finally
        {
            CURATOR_FRAMEWORK.delete().deletingChildrenIfNeeded().forPath("/foo2");
        }
    }

}
