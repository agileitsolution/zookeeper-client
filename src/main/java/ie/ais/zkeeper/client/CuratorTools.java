package ie.ais.zkeeper.client;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.CuratorFrameworkFactory.Builder;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import framework.CrudExamples;
/**
 * @author qindongliang
 * Curator operation zookeeper
 * Basic examples
 * **/
public class CuratorTools {

    private static final Logger LOG = LoggerFactory.getLogger(CuratorTools.class);
    static CuratorFramework zkclient=null;
    static String nameSpace="php";
    static {

        String zkhost="127.0.0.1:2181";//ZK host
        RetryPolicy rp=new ExponentialBackoffRetry(1000, 3);//Retry mechanism
        Builder builder = CuratorFrameworkFactory.builder().connectString(zkhost)
                .connectionTimeoutMs(5000)
                .sessionTimeoutMs(5000)
                .retryPolicy(rp);
        builder.namespace(nameSpace);
        CuratorFramework zclient = builder.build();
        zkclient=zclient;
        zkclient.start();// Implemented in the front
        zkclient.newNamespaceAwareEnsurePath("/"+nameSpace);

    }

    public static void main(String[] args)throws Exception {
        CuratorTools ct=new  CuratorTools();
        ct.getListChildren("/zk");
        ct.upload("/jianli/123.txt", "/Users/skuz0001/wsp2/_log4j2/_new/zkclient/src/main/resources/123.txt");
        ct.createrOrUpdate("/zk/cc334/zzz","c");
        //ct.delete("/qinb/bb");
        ct.checkExist("/zk");
        ct.read("/jianli/123.txt");

        ct.createrOrUpdate("/corp_a1/ban123/ban456/dacabc/shop1","shop1 permissions");
        ct.createrOrUpdate("/corp_a1/ban789/ban444/dacdef/shop2","shop1 permissions");
        ct.read("/corp_a1/ban123");
        ct.read("/corp_a1/ban789");
        ct.read("/corp_a1");

        zkclient.close();


    }

    /**
     * Create or update a node
     *
     * @Param path path
     * @Param content content
     * **/
    public void createrOrUpdate(String path,String content)throws Exception{

        zkclient.newNamespaceAwareEnsurePath(path).ensure(zkclient.getZookeeperClient());
        zkclient.setData().forPath(path,content.getBytes());
        LOG.debug("Added successfully!!!");

    }

    /**
     * Deleting ZK nodes
     * @The path param path delete node
     *
     * **/
    public void delete(String path)throws Exception{
        zkclient.delete().guaranteed().deletingChildrenIfNeeded().forPath(path);
        LOG.debug("Successfully deleted!");
    }


    /**
     * To judge whether a path exists
     * @param path
     * **/
    public void checkExist(String path)throws Exception{

        if(zkclient.checkExists().forPath(path)==null){
            LOG.debug("Path does not exist!");
        }else{
            LOG.debug("The path already exists!");
        }

    }

    /**
     * The read path
     * @param path
     * **/
    public void read(String path)throws Exception{


        String data=new String(zkclient.getData().forPath(path),"gbk");

        LOG.debug("Read data:"+data);

    }


    /**
     * @Param path path
     * Get all the sub documents a node next
     * */
    public void getListChildren(String path)throws Exception{

        List<String> paths=zkclient.getChildren().forPath(path);
        for(String p:paths){
            LOG.debug(p);
        }

    }

    /**
     * @The path to the param zkPath on ZK
     * @Param localpath on the local file path
     *
     * **/
    public void upload(String zkPath,String localpath)throws Exception{

        createrOrUpdate(zkPath, "");//Create a path
        byte[] bs= FileUtils.readFileToByteArray(new File(localpath));
        zkclient.setData().forPath(zkPath, bs);
        LOG.debug("Upload file successfully!");


    }





}