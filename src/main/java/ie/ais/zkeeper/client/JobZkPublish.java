package ie.ais.zkeeper.client;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

public class JobZkPublish {

    private static final Logger LOG = LoggerFactory.getLogger(JobZkPublish.class);
    public static final String ZK_SERVER =  "localhost";
    public static void main(String[] args) throws Exception {

        CuratorFramework client = CuratorFrameworkFactory.newClient(ZK_SERVER, new ExponentialBackoffRetry(1000, 3));
        client.start();

        // create my node
        String hostname = InetAddress.getLocalHost().getHostName();

        String jobs = "/urmia/1/" + hostname + "/jobs";

//        JobExec lsJob = new JobExec.Shell("ls");
//        JobDefinition.Phase lsJobPhase = new JobDefinition.Phase(lsJob);
//        JobDefinition jobDef = new JobDefinition("tck", Lists.newArrayList(lsJobPhase));
//
//        byte[] lsJobBytes = jobDef.toString().getBytes();
//
//        if (client.checkExists().forPath(jobs) == null)
//            client.create().withMode(CreateMode.PERSISTENT).forPath(jobs);
//
//        String i = new RandomUuidImpl().next();
//
//        LOG.info("creating job: {}", i);
//        String p = jobs + "/" + i;
//
//        client.create().withMode(CreateMode.PERSISTENT).forPath(p, lsJobBytes);
//
//        String ip = p + "/live/in";
//
//        QueueBuilder<JobInput> builder = QueueBuilder.builder(client, null, serializer, ip);
//
//        Thread.sleep(500);
//
//        DistributedQueue<JobInput> q = builder.buildQueue();
//
//        q.start();
//
//        q.put(new LineJobInput("/"));
//        q.put(new LineJobInput("/tmp"));
//        q.put(END);
//
//        q.flushPuts(1, TimeUnit.SECONDS);

        Thread.sleep(1000);

    }
}
