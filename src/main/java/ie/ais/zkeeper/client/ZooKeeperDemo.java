package ie.ais.zkeeper.client;


import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.zookeeper.data.Stat;

public class ZooKeeperDemo {
    private static final Logger LOG = LoggerFactory.getLogger(ZooKeeperDemo.class);
    public static CountDownLatch connectedSignal = new CountDownLatch(1);
    public static ZooKeeper zooKeeper;

    public static void main(String[] args) throws IOException {
        final String path = "/MyFirstZnode";
        try {
            zooKeeper = new ZooKeeper("127.0.0.1:2181", 2000, new ZooKeeperWatcher());
            // data in byte array
            byte[] data = "My first zookeeper app".getBytes(); // Declare data
            String createdNodePath = null;

            try {
                //create a node with path path, data as data, ACL giving everyone rights on this node and create mode as PERSISTENT
                createdNodePath = zooKeeper.create( path, data, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
                LOG.info("Node created with path: " + createdNodePath);

            } catch (KeeperException.NodeExistsException e)
            {
                LOG.debug("Node already exist. It is created in PERSISTENT mode previously");
            }

            //Delete a node with path "/demo" and version as -1 means don't care for current version
//            zooKeeper.delete(createdNodePath, -1);

            Stat stat = znode_exists(path); // Stat checks the path of the znode

            if(stat != null) {
                LOG.debug("Node exists and the node version is " +  stat.getVersion());
                byte[] b = zooKeeper.getData(path, new Watcher() {

                    public void process(WatchedEvent we) {

                        if (we.getType() == Event.EventType.None) {
                            switch(we.getState()) {
                                case Expired:
                                    connectedSignal.countDown();
                                    break;
                            }
                        }
//                        else {
//
//                            try {
//                                byte[] bn = zooKeeper.getData(path,
//                                        false, null);
//                                String data = new String(bn,
//                                        "UTF-8");
//                                LOG.debug(data);
//                                connectedSignal.countDown();
//
//                            } catch(Exception ex) {
//                                LOG.error(ex.getMessage());
//                            }
//                        }
                    }
                }, null);

                String dataFromNode = new String(b, "UTF-8");
                LOG.debug(dataFromNode);
                connectedSignal.await();

            } else {
                LOG.debug("Node does not exists");
            }

        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Method to check existence of znode and its status, if znode is available.
    public static Stat znode_exists(String path) throws
            KeeperException,InterruptedException {
        return zooKeeper.exists(path, true);
    }

    public static class ZooKeeperWatcher implements Watcher {

        public void process(WatchedEvent event) {
            //process the event
            if (event.getState() == Event.KeeperState.SyncConnected) {
                connectedSignal.countDown();
            }
        }
    }
}

